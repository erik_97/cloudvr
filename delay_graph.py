import matplotlib.pyplot as plt
import numpy as np


width = 252


def set_size(width, fraction=1):
    """Set figure dimensions to avoid scaling in LaTeX.

    Parameters
    ----------
    width: float
            Document textwidth or columnwidth in pts
    fraction: float, optional
            Fraction of the width which you wish the figure to occupy

    Returns
    -------
    fig_dim: tuple
            Dimensions of figure in inches
    """
    # Width of figure (in pts)
    fig_width_pt = width * fraction

    # Convert from pt to inches
    inches_per_pt = 1 / 72.27

    # Golden ratio to set aesthetic figure height
    # https://disq.us/p/2940ij3
    golden_ratio = (5**.5 - 1) / 2

    # Figure width in inches
    fig_width_in = fig_width_pt * inches_per_pt
    # Figure height in inches
    fig_height_in = fig_width_in * golden_ratio

    fig_dim = (fig_width_in, fig_height_in)

    return fig_dim


with open("delays.txt") as fullfile, open("delays_half.txt") as halffile:
    full = np.array([int(line) for line in fullfile.read().splitlines()])
    half = np.array([int(line) for line in halffile.read().splitlines()])
    x = list(range(1, len(full) + 1))
    print(len(full))
    print(len(x))

    fig, ax = plt.subplots(1, 1, figsize=set_size(width))
    fig.suptitle('computational delay time samples',
                 fontsize=8)

    # ax.boxplot(data)

    # ax.set_title('boxplot distribution with interquartile range of 1.5')
    ax.set_xlabel('sample number [-]', fontsize=8)
    ax.set_ylabel('rendering time [$\mu$s]', fontsize=8)

    # plt.errorbar(x, y, e, linestyle='None', marker='^')
    # plt.boxplot(box, whis=(0, 100))
    plt.scatter(x, full, c="red", s=3, label='original samples')
    plt.scatter(x, half, c="blue", s=3, label='halved samples')
    ax.legend()
    plt.show()
    fig.savefig('delay_samples.png', bbox_inches="tight")
