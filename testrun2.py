import os
import sys
from time import sleep

client_amt = int(sys.argv[1])
test_runtime = int(sys.argv[2])
server = sys.argv[3]

if client_amt <= 0 or test_runtime < 0:
    raise ValueError("argument must be positive")
print("running {} clients".format(client_amt))


for i in range(client_amt):
    os.system(
        "cargo run --release --bin client -- "
        "-o ./logs/xlog{}.txt -r {} -s {} -w {} &".format(i + 1, test_runtime, server, 400 * (i + 1)))
    sleep(test_runtime + 10)
