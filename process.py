import sys
from contextlib import ExitStack
from statistics import mean, stdev
import matplotlib.pyplot as plt
import numpy as np
client_amt = int(sys.argv[1])


def set_size(width, fraction=1):
    """Set figure dimensions to avoid scaling in LaTeX.

    Parameters
    ----------
    width: float
            Document textwidth or columnwidth in pts
    fraction: float, optional
            Fraction of the width which you wish the figure to occupy

    Returns
    -------
    fig_dim: tuple
            Dimensions of figure in inches
    """
    # Width of figure (in pts)
    fig_width_pt = width * fraction

    # Convert from pt to inches
    inches_per_pt = 1 / 72.27

    # Golden ratio to set aesthetic figure height
    # https://disq.us/p/2940ij3
    golden_ratio = (5**.5 - 1) / 2

    # Figure width in inches
    fig_width_in = fig_width_pt * inches_per_pt
    # Figure height in inches
    fig_height_in = fig_width_in * golden_ratio

    fig_dim = (fig_width_in, fig_height_in)

    return fig_dim


def filenames_generator(upper):
    for i in range(1, upper + 1):
        yield ["./logs/experiment1-home/log{}_{}.txt".format(i, j) for j in range(1, i + 1)]


width = 252
data = []

for amt, filenames in enumerate(filenames_generator(client_amt)):
    print("means for {} client(s)".format(amt + 1))
    with ExitStack() as stack:
        files = [stack.enter_context(open(fname)) for fname in filenames]
        values = []
        for file in files:
            values = values + [int(line) for line in file.read().splitlines()]
        # print(mean(values), stdev(values))
        # # means.append(mean(values))
        # # deviations.append(stdev(values))
        data.append(values)

fig, ax = plt.subplots(1, 1, figsize=set_size(width))
fig.suptitle('failed requests per second',
             fontsize=8)

ax.boxplot(data)

# ax.set_title('boxplot distribution with interquartile range of 1.5')
ax.set_xlabel('client amount [client]', fontsize=8)
ax.set_ylabel('failed [request / second]', fontsize=8)

# plt.errorbar(x, y, e, linestyle='None', marker='^')
# plt.boxplot(box, whis=(0, 100))
plt.boxplot(data)
# plt.show()
fig.savefig('exp1-home.png', bbox_inches="tight")
