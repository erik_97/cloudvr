# CloudVR
Testing networks for future Cloud based Virtual Reality Applications

CloudVR is an UDP testing tool to test network bandwidth and latency requirements for future Virtual Reality application running in the cloud instead of on a local workstation. For this purpose a networking protocol was designed, available [here](paper.pdf).

# Installation
### Rust installation
To install Rust, run  
`$ curl --proto '=https' --tlsv1.2 -sSf https://sh.rustup.rs | sh`  
or follow the instructions at [rust-lang.org](https://www.rust-lang.org/tools/install).

### Clone Repository
Clone this repository with  
`$ git clone git@gitlab.com:erik_97/cloudvr.git`

# Using the application
### Run application
To run the application, run  
`$ cargo run --bin client -- [options]`  
for the client application and  
`$ cargo run --bin server -- [options]`  
for the server application.

IMPORTANT: add the `--release` flag to the run command to enable compiler optimizations, which will lead to longer compile times.

To display the available options use the `-h` option.

Get options available for the client application:
`$ cargo run --bin client -- -h`

```
USAGE:
    client [OPTIONS]

FLAGS:
    -h, --help       Prints help information
    -V, --version    Prints version information

OPTIONS:
    -o <FILE>           Sets a custom output log file
    -d <delay>          delay requirement in miliseconds [default: 20]
    -e <epsilon>        max % exceeding delay requirement [default: 5]
    -f <fps>            target frames per second [default: 100]
    -p <pixels>         pixels settings from server [default: 20]
    -r <runtime>        duration of the test run in seconds [default: 60]
    -s <socket>         IP/UDP address of the server [default: 127.0.0.1:9999]
    -t <tall>           screen height in pixels [default: 400]
    -w <wide>           screen width in pixels [default: 400]
```

Get options available for the server application: `$ cargo run --bin server -- -h`

```
USAGE:
    server [OPTIONS]

FLAGS:
    -h, --help       Prints help information
    -V, --version    Prints version information

OPTIONS:
    -d <FILE>               Processing delay randomly sampled from FILE
    -c <computation>        computational delay in microseconds [default: 0]
    -p <pixels>             width and height of miniframe <= 20 [default: 20]
    -s <socket>             The UDP port to to server [default: 127.0.0.1:9999]
    -t <threads>            sets the config file to use [default: 4]
```

The binary executable files can be found in the `target/debug` directory for regular builds and `target/release` for optimized builds. If not already build, compilation is done by running the `$ cargo build` or `$ cargo build --release` command.

The application will print the amount of failed requests to the screen or to the file specified by the `-o <FILE>` option.

### Examples
#### On the loopback interface with default options
To run the application on the loopback interface with default values, execute  
`$ cargo run --bin server`  
for the server, and  
`$ cargo run --bin client`  
for the client.

#### On the loopback interface with custom options
This example shows how to run the application with custom values:
- Socket reachable on the loopback interface at port 5555 (server)
- Connected to server on the loopback interface at port 5555 (client)
- 60 frames per second (client)
- Log to home directory (client)

Start the server: `$ cargo run --bin server -- -s 127.0.0.1:5555`  
Start the client: `$ cargo run --bin client -- -s 127.0.0.1:5555 -f 60 -o ~/log.txt`


#### On a local network with custom options
- Thread pool with 8 threads (server)
- Socket reachable on all interface at port 7777 (server)
- Connected to server on 192.168.2.88 at port 7777 (client)
- Run for 200 seconds (client)
- Running the release/optimized builds

Start the server `$ cargo run --release --bin server -- -t 8 -s 0.0.0.0:7777`  
Start the client `$ cargo run --release --bin client -- -s 192.168.2.88:7777 -r 200`  
