with open("delays.txt") as readfile, open("delays_half.txt", 'a') as writefile:
    values = [int(line) for line in readfile.read().splitlines()]
    for value in values:
        writefile.write(f'{value // 2}\n')
