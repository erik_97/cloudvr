import os
import sys
from time import sleep

client_amt = int(sys.argv[1])
test_runtime = int(sys.argv[2])
server = sys.argv[3]

if client_amt <= 0 or test_runtime < 0:
    raise ValueError("argument must be positive")
print("running {} clients".format(client_amt))


for i in range(client_amt):
    for j in range(i + 1):
        os.system(
            "cargo run --release --bin client -- "
            "-o ./logs/log{}_{}.txt -r {} -s {} &".format(i + 1, j + 1, test_runtime, server))
        sleep(0.01)
    sleep(test_runtime + 10)
