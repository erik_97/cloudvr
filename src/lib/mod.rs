use serde::{Deserialize, Serialize};

pub mod frame;
pub mod miniframe;
pub mod response;
mod utils;

#[derive(Clone, Debug, PartialEq, Default, Serialize, Deserialize)]
pub struct Identifier {
    pub id: u64,
    pub sequence: u64,
}

impl Identifier {
    pub fn new(id: u64, sequence: u64) -> Identifier {
        Identifier { id, sequence }
    }
}

#[derive(Debug, Default, Serialize, Deserialize)]
pub struct Position {
    x: i64,
    y: i64,
    z: i64,
}

#[derive(Debug, Default, Serialize, Deserialize)]
pub struct Orientation {
    pitch: f64,
    roll: f64,
    yaw: f64,
}

#[derive(Copy, Clone, Debug, Default, Serialize, Deserialize)]
pub struct Resolution {
    pub horizontal: u64,
    pub vertical: u64,
}

impl Resolution {
    pub fn new(h: u64, v: u64) -> Resolution {
        Resolution {
            horizontal: h,
            vertical: v,
        }
    }
}

#[derive(Serialize, Deserialize, Debug, Default)]
pub struct FrameRequest {
    pub identifier: Identifier,
    pub position: Position,
    pub orientation: Orientation,
    pub resolution: Resolution,
}

impl FrameRequest {
    pub fn create(input: &[u8]) -> Result<FrameRequest, &'static str> {
        let chunks: Vec<&[u8]> = input.chunks(8).collect();
        let id = utils::read_be_i64(chunks[0])? as u64;
        let sequence = utils::read_be_i64(chunks[1])? as u64;
        let x = utils::read_be_i64(chunks[2])?;
        let y = utils::read_be_i64(chunks[3])?;
        let z = utils::read_be_i64(chunks[4])?;
        let pitch = utils::read_be_f64(chunks[5])?;
        let roll = utils::read_be_f64(chunks[6])?;
        let yaw = utils::read_be_f64(chunks[7])?;
        let horizontal = utils::read_be_i64(chunks[8])? as u64;
        let vertical = utils::read_be_i64(chunks[9])? as u64;

        Ok(FrameRequest {
            identifier: Identifier { id, sequence },
            position: Position { x, y, z },
            orientation: Orientation { pitch, roll, yaw },
            resolution: Resolution {
                horizontal,
                vertical,
            },
        })
    }
}
