use std::convert::TryInto;

pub fn read_be_i64(input: &[u8]) -> Result<i64, &'static str> {
    if input.len() < std::mem::size_of::<i64>() {
        return Err("Not enough bytes");
    }
    let (int_bytes, _) = input.split_at(std::mem::size_of::<i64>());
    let bytes = int_bytes.try_into();
    match bytes {
        Ok(s) => Ok(i64::from_be_bytes(s)),
        Err(_) => Err("Could not convert"),
    }
}

pub fn read_be_u64(input: &[u8]) -> Result<u64, &'static str> {
    if input.len() < std::mem::size_of::<u64>() {
        return Err("Not enough bytes");
    }
    let (int_bytes, _) = input.split_at(std::mem::size_of::<u64>());
    let bytes = int_bytes.try_into();
    match bytes {
        Ok(s) => Ok(u64::from_be_bytes(s)),
        Err(_) => Err("Could not convert"),
    }
}

pub fn read_be_u16(input: &[u8]) -> Result<u16, &'static str> {
    if input.len() < std::mem::size_of::<u16>() {
        return Err("Not enough bytes");
    }
    let (int_bytes, _) = input.split_at(std::mem::size_of::<u16>());
    let bytes = int_bytes.try_into();
    match bytes {
        Ok(s) => Ok(u16::from_be_bytes(s)),
        Err(_) => Err("Could not convert"),
    }
}

pub fn read_be_f64(input: &[u8]) -> Result<f64, &'static str> {
    if input.len() < std::mem::size_of::<f64>() {
        return Err("Not enough bytes");
    }
    let (int_bytes, _) = input.split_at(std::mem::size_of::<f64>());
    let bytes = int_bytes.try_into();
    match bytes {
        Ok(s) => Ok(f64::from_be_bytes(s)),
        Err(_) => Err("Could not convert"),
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_read_i64_ok() {
        let binary: Vec<u8> = vec![0, 0, 0, 0, 0, 0, 1, 0];
        let expected = 256;
        assert_eq!(read_be_i64(&binary).unwrap(), expected);
    }

    #[test]
    fn test_read_i64_not_enough_bytes() {
        let binary: Vec<u8> = vec![0, 0, 3];
        assert!(read_be_i64(&binary).is_err());
    }

    #[test]
    fn test_read_u64_ok() {
        let binary: Vec<u8> = vec![0, 0, 0, 0, 0, 0, 2, 0];
        let expected = 512;
        assert_eq!(read_be_u64(&binary).unwrap(), expected);
    }

    #[test]
    fn test_read_u64_not_enough_bytes() {
        let binary: Vec<u8> = vec![0, 0, 3];
        assert!(read_be_u64(&binary).is_err());
    }

    #[test]
    fn test_read_u16_ok() {
        let binary: Vec<u8> = vec![2, 0];
        let expected = 512;
        assert_eq!(read_be_u16(&binary).unwrap(), expected);
    }

    #[test]
    fn test_read_u16_not_enough_bytes() {
        let binary: Vec<u8> = vec![3];
        assert!(read_be_u16(&binary).is_err());
    }

    #[test]
    fn test_read_f64_ok() {
        let binary: Vec<u8> = vec![64, 32, 0, 0, 0, 0, 0, 0];
        let result: f64 = read_be_f64(&binary).unwrap();
        let expected: f64 = 8.0;
        assert_eq!(result, expected);
    }

    #[test]
    fn test_read_f64_not_enough_bytes() {
        let binary: Vec<u8> = vec![0, 0, 3];
        assert!(read_be_f64(&binary).is_err());
    }
}
