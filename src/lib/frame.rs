use super::miniframe::{MiniFrame, MiniFrameIterator};
use super::FrameRequest;

// pub const MINIFRAME_SIZE: usize = 2;
// pub const MINIFRAME_PIXEL_AMOUNT: usize = MINIFRAME_SIZE * MINIFRAME_SIZE;

#[derive(Debug)]
pub struct Frame {
    // Full frame
    pub request: FrameRequest,
    pub num_of_miniframes: u16,
    pub m_pixel_length: usize,
    pub m_pixel_amount: usize,
    pub pixels: Vec<u8>,
}

impl Frame {
    pub fn iter_miniframes(&self) -> MiniFrameIterator {
        MiniFrameIterator {
            frame: self,
            current: 0,
        }
    }

    pub fn miniframe(&self, number: u16) -> MiniFrame {
        MiniFrame {
            identifier: self.request.identifier.clone(),
            index: number,
            pixels: self.pixels.clone(),
        }
    }

    pub fn render_frame(request: FrameRequest, m_pixel_length: usize) -> Frame {
        // The place to call an external rendering service
        let m_pixel_amount = m_pixel_length * m_pixel_length;

        let num_of_pixels = request.resolution.horizontal * request.resolution.vertical;

        let num_of_miniframes = (num_of_pixels / m_pixel_amount as u64) as u16;
        let pixels = vec![0; m_pixel_amount];

        Frame {
            request,
            num_of_miniframes,
            m_pixel_length,
            m_pixel_amount,
            pixels,
        }
    }
}
