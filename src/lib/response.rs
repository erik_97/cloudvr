use super::FrameRequest;

#[derive(Debug)]
pub struct ResponseBuilder {
    id: u64,
    seq: u64,
    m_bytes: usize,
    current: u16,
    amt: u16,
    size: u16,
}

impl ResponseBuilder {
    pub fn new(request: FrameRequest, m_pixel_length: usize, size: u16) -> ResponseBuilder {
        let m_pixel_amount = m_pixel_length * m_pixel_length;
        let num_of_pixels = request.resolution.horizontal * request.resolution.vertical;
        let num_of_miniframes = (num_of_pixels / m_pixel_amount as u64) as u16;

        ResponseBuilder {
            id: request.identifier.id,
            seq: request.identifier.sequence,
            m_bytes: 3 * m_pixel_amount,
            current: 0,
            amt: num_of_miniframes,
            size,
        }
    }
}

impl Iterator for ResponseBuilder {
    type Item = ResponseIterator;

    fn next(&mut self) -> Option<Self::Item> {
        if self.current < self.amt {
            let start = self.current;
            self.current += self.size;
            let end = if self.current > self.amt {
                self.amt
            } else {
                self.current
            };
            Some(ResponseIterator {
                id: self.id,
                seq: self.seq,
                m_bytes: self.m_bytes,
                current: start,
                stop: end,
                bytes: vec![0; 3 * self.m_bytes],
            })
        } else {
            None
        }
    }
}

#[derive(Debug)]
pub struct ResponseIterator {
    id: u64,
    seq: u64,
    pub m_bytes: usize,
    current: u16,
    pub stop: u16,
    bytes: Vec<u8>,
}

impl ResponseIterator {
    pub fn new(request: FrameRequest, m_pixel_length: usize) -> ResponseIterator {
        let m_pixels = m_pixel_length * m_pixel_length;
        let m_bytes = 3 * m_pixels;
        let num_of_pixels = request.resolution.horizontal * request.resolution.vertical;
        let num_of_miniframes = (num_of_pixels / m_pixels as u64) as u16;

        ResponseIterator {
            id: request.identifier.id,
            seq: request.identifier.sequence,
            m_bytes: m_bytes,
            current: 0,
            stop: num_of_miniframes,
            bytes: vec![0; m_bytes],
        }
    }
}

impl Iterator for ResponseIterator {
    type Item = Vec<u8>;

    fn next(&mut self) -> Option<Self::Item> {
        // let now = Instant::now();
        if self.current < self.stop {
            let mut bytes = Vec::new();
            bytes.extend_from_slice(&self.id.to_be_bytes());
            bytes.extend_from_slice(&self.seq.to_be_bytes());
            bytes.extend_from_slice(&self.current.to_be_bytes());
            bytes.extend_from_slice(self.bytes.as_slice());
            self.current += 1;
            // println!("next took {:?}", Instant::elapsed(&now));
            Some(bytes)
        } else {
            None
        }
    }
}
