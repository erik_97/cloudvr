use super::frame::Frame;
use super::utils::*;
use super::Identifier;

#[derive(Clone, Debug, PartialEq)]
pub struct MiniFrame {
    pub identifier: Identifier,
    pub index: u16,
    pub pixels: Vec<u8>,
}

impl MiniFrame {
    pub fn into_bytes(mut self) -> Vec<u8> {
        let mut bytes: Vec<u8> = Vec::new();
        bytes.extend_from_slice(&self.identifier.id.to_be_bytes());
        bytes.extend_from_slice(&self.identifier.sequence.to_be_bytes());
        bytes.extend_from_slice(&self.index.to_be_bytes());
        bytes.append(self.pixels.as_mut());

        bytes
    }

    pub fn from_bytes(bytes: &[u8]) -> MiniFrame {
        let id = read_be_u64(&bytes[0..8]).unwrap();
        let sequence = read_be_u64(&bytes[8..16]).unwrap();
        let index = read_be_u16(&bytes[16..18]).unwrap();
        let mut pixels: Vec<u8> = Vec::new();
        pixels.extend_from_slice(&bytes[18..]);

        MiniFrame {
            identifier: Identifier { id, sequence },
            index,
            pixels,
        }
    }
}

pub struct MiniFrameIterator<'a> {
    pub frame: &'a Frame,
    pub current: u16,
}

impl<'a> Iterator for MiniFrameIterator<'a> {
    type Item = MiniFrame;

    fn next(&mut self) -> std::option::Option<Self::Item> {
        if self.current < self.frame.num_of_miniframes {
            let miniframe = self.frame.miniframe(self.current);
            self.current += 1;
            Some(miniframe)
        } else {
            None
        }
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn miniframe_to_bytes() {
        let identifier = Identifier {
            id: 777,
            sequence: 14,
        };

        let miniframe = MiniFrame {
            identifier,
            index: 9,
            pixels: vec![0, 1, 2, 4, 8, 16, 32, 64, 128],
        };
        let binary: &[u8] = &[
            0, 0, 0, 0, 0, 0, 3, 9, // id
            0, 0, 0, 0, 0, 0, 0, 14, // sequence
            0, 9, // index
            0, 1, 2, 4, 8, 16, 32, 64, 128,
        ]; // pixels
        let encoded = miniframe.into_bytes();
        assert_eq!(encoded.as_slice(), binary);
    }

    #[test]
    fn bytes_to_miniframe() {
        let identifier = Identifier {
            id: 777,
            sequence: 14,
        };

        let miniframe = MiniFrame {
            identifier,
            index: 9,
            pixels: vec![0, 1, 2, 4, 8, 16, 32, 64, 128],
        };
        let binary: &[u8] = &[
            0, 0, 0, 0, 0, 0, 3, 9, // id
            0, 0, 0, 0, 0, 0, 0, 14, // sequence
            0, 9, // index
            0, 1, 2, 4, 8, 16, 32, 64, 128,
        ]; // pixels
        let decoded = MiniFrame::from_bytes(binary);
        assert_eq!(decoded, miniframe);
    }

    #[test]
    fn miniframe_to_bytes_to_miniframe() {
        let identifier = Identifier {
            id: 777,
            sequence: 14,
        };

        let miniframe = MiniFrame {
            identifier,
            index: 9,
            pixels: vec![0, 1, 2, 4, 8, 16, 32, 64, 128],
        };
        let original = miniframe.clone();
        let encoded = miniframe.into_bytes();
        let decoded = MiniFrame::from_bytes(&encoded);
        assert_eq!(decoded, original);
    }

    #[test]
    fn bytes_to_miniframe_to_bytes() {
        let binary: Vec<u8> = vec![
            0, 0, 0, 0, 0, 0, 3, 9, // id
            0, 0, 0, 0, 0, 0, 0, 14, // sequence
            0, 9, // index
            0, 1, 2, 4, 8, 16, 32, 64, 128,
        ]; // pixels
        let original = binary.clone();
        let miniframe = MiniFrame::from_bytes(&binary);
        let encoded = miniframe.into_bytes();
        assert_eq!(encoded, original);
    }
}
