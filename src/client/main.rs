extern crate clap;
extern crate crossbeam_channel;

use clap::{App, Arg};
use crossbeam_channel::{unbounded, Receiver, Sender};
use libcloudvr::{miniframe::MiniFrame, FrameRequest, Identifier, Resolution};

use std::collections::{HashMap, HashSet, VecDeque};
use std::fs::File;
use std::io::prelude::*;
use std::net::{SocketAddr, UdpSocket};
use std::rc::Rc;
// use std::sync::mpsc::{channel, Receiver, Sender};
use std::thread;
use std::time::{Duration, Instant};

fn main() -> std::io::Result<()> {
    let matches = App::new("cloudvr-server")
        .version("0.1")
        .author("Erik van Bennekum")
        .about("Cloud VR emulator")
        .args(&[
            Arg::with_name("socket")
                .about("IP/UDP address of the server")
                .default_value("127.0.0.1:9999")
                .short('s'),
            Arg::with_name("fps")
                .about("target frames per second")
                .default_value("100")
                .short('f'),
            Arg::with_name("delay")
                .about("delay requirement in miliseconds")
                .default_value("20")
                .short('d'),
            Arg::with_name("epsilon")
                .about("max % exceeding delay requirement")
                .default_value("5")
                .short('e'),
            Arg::with_name("pixels")
                .about("pixels settings from server")
                .default_value("20")
                .short('p'),
            Arg::with_name("tall")
                .about("screen height in pixels")
                .default_value("400")
                .short('t'),
            Arg::with_name("wide")
                .about("screen width in pixels")
                .default_value("400")
                .short('w'),
            Arg::new("output")
                .short('o')
                .value_name("FILE")
                .about("Sets a custom output log file")
                .takes_value(true),
            Arg::with_name("runtime")
                .about("duration of the test run in seconds")
                .default_value("60")
                .short('r'),
        ])
        .get_matches();

    /* PROCESS COMMAND ARGUMENTS */
    // socket parsing
    let server_address: SocketAddr = matches.value_of_t("socket").unwrap();
    let socket_a = UdpSocket::bind("0.0.0.0:0").expect("Could not bind to this socket");
    // socket_a.connect(server_address).unwrap();
    let socket_b = socket_a.try_clone().unwrap();

    // fps parsing
    let fps: u32 = matches.value_of_t("fps").unwrap();
    assert!(fps > 0, "fps must be positive");
    let _fps_delay = Duration::new(0, 1_000_000_000 / fps);

    // delay parsing
    let max_delay = Duration::from_millis(matches.value_of_t("delay").unwrap());

    // epsilon parsing
    let epsilon: f64 = matches.value_of_t("epsilon").unwrap();

    // get resolution from command line
    let m_pixels: u64 = matches.value_of_t("pixels").unwrap();
    let width: u64 = matches.value_of_t("wide").unwrap();
    let height: u64 = matches.value_of_t("tall").unwrap();
    assert!(
        width % m_pixels == 0 && height % m_pixels == 0,
        "width and height should be multiples of pixels"
    );
    let resolution = Resolution::new(height, width);
    let num_of_miniframes = width * height / m_pixels / m_pixels;
    println!("{} miniframes in a full frame", num_of_miniframes);

    // get test running time
    let duration: u64 = matches.value_of_t("runtime").unwrap();

    /* SHARED DATA STRUCTURES */
    let id = 1u64;
    let (sender_tx, sender_rx) = unbounded();
    let (receiver_tx, receiver_rx) = unbounded();
    let (report_tx, report_rx) = unbounded();

    // Create separate thread for sending periodic requests
    thread::Builder::new()
        .name("sender".to_string())
        .spawn(move || periodic_sender(socket_a, sender_tx, fps, id, resolution, server_address))?;

    thread::Builder::new()
        .name("receiver".to_string())
        .spawn(move || {
            receiver(socket_b, receiver_tx, id);
        })?;

    thread::Builder::new()
        .name("processing".to_string())
        .spawn(move || {
            processing(sender_rx, receiver_rx, report_tx, max_delay);
        })?;

    // reporting thread
    let allowed_missing = (epsilon * num_of_miniframes as f64) as u64 / 100;
    println!("allowed missing {}", allowed_missing);

    let mut file_opt = None;

    if let Some(filename) = matches.value_of("output") {
        // let filename: String = matches.value_of_t_or_exit("output");
        println!("output to file");
        let file = File::create(&filename)
            .unwrap_or_else(|_| panic!("could not create file {}", &filename));
        // let header = "failed requests\ttotal requests per second\n".to_string();
        // file.write_all(header.as_bytes())
        //     .expect("could not wire to file");
        file_opt = Some(Rc::new(file));
    }

    // counter used to determine runtime
    let mut counter: u64 = 0;

    loop {
        let mut total_delay_sum = 0;
        let mut total_delay_amt = 0;
        let mut failed_requests = 0;
        let mut total_missing = 0;

        for _ in 0..fps {
            if let Ok(report) = report_rx.recv() {
                total_delay_sum += report.delay_sum;
                total_delay_amt += report.delay_amt;
                let missing = num_of_miniframes - report.delay_amt;
                // println!("missing: {}", missing);
                total_missing += missing;
                if missing > allowed_missing {
                    failed_requests += 1;
                }
            } else {
                break;
            }
        }
        counter += 1;
        let avg_delay = total_delay_sum.checked_div(total_delay_amt);
        let avg_missing = total_missing as f64 / (fps * num_of_miniframes as u32) as f64 * 100f64;

        if let Some(mut file) = file_opt.as_deref() {
            let data = format!("{}\n", failed_requests);
            file.write_all(data.as_bytes()).unwrap();
        } else {
            println!("requests failed/total: {}/{}", failed_requests, fps);
            if let Some(avg_delay) = avg_delay {
                println!(
                    "average delay: {}us, average late/missing responses {:.2}%",
                    avg_delay, avg_missing
                );
            } else {
                println!("average delay: -  , average late/missing responses 100%");
            }
        }

        if counter == duration && duration != 0 {
            std::process::exit(0);
        }
    }
}

fn create_request(id: u64, sequence: u64, resolution: Resolution) -> FrameRequest {
    let identifier = libcloudvr::Identifier::new(id, sequence);
    FrameRequest {
        identifier,
        resolution,
        ..Default::default()
    }
}

#[derive(Debug)]
struct Report {
    delay_sum: u64, // in microseconds
    delay_amt: u64, // amount of received
}

fn periodic_sender(
    socket: UdpSocket,
    channel: Sender<(u64, Instant)>,
    fps: u32,
    id: u64,
    resolution: Resolution,
    destination: SocketAddr,
) {
    let mut sequence: u64 = 1;
    let mut loop_time = Instant::now();
    let loop_us = 1_000_000_000 / fps;
    let loop_duration = Duration::new(0, loop_us);
    let sleep_threshold = Duration::new(0, 1_000_000);

    loop {
        // sleep-wait until almost loop time has passed
        while loop_time.elapsed() < loop_duration - sleep_threshold {
            thread::sleep(Duration::new(0, 1_000_000));
        }

        // busy-wait until loop time has passed
        // this is done for more precise loop time
        while loop_time.elapsed() < loop_duration {
            thread::sleep(Duration::new(0, 1_000));
        }
        // println!("Loop time {:?}", Instant::elapsed(&loop_time));
        // record start time for next loop
        loop_time = Instant::now();

        // create request
        let request = create_request(id, sequence, resolution);
        let binary_request = bincode::config().big_endian().serialize(&request).unwrap();

        // record time of sending
        let sent_time = Instant::now();

        // send request
        socket.send_to(&binary_request, destination).unwrap();

        // send sequence and time into channel
        channel.send((sequence, sent_time)).unwrap();

        // PART 4: increase sequence
        sequence += 1;
    }
}

fn receiver(socket: UdpSocket, channel: Sender<(u64, u16, Instant)>, id: u64) {
    let mut buf = [0; 1500];

    loop {
        // receive datagram
        while let Ok(amt) = socket.recv(&mut buf) {
            if amt >= std::mem::size_of::<(Identifier, u16)>() {
                // record receive time
                let recv_time = Instant::now();

                // convert bytes to miniframe
                let mf = MiniFrame::from_bytes(&buf[..amt]);

                if mf.identifier.id == id {
                    channel
                        .send((mf.identifier.sequence, mf.index, recv_time))
                        .unwrap();
                }
            }
        }
    }
}

// fn send_logger(channel: Receiver<(u64, Instant)>, send_log: WriteHandle<u64, Box<Instant>>) {
//     loop {
//         while let Ok((seq, sent_time)) = channel.recv() {
//             send_log.insert(seq, Box::new(sent_time));
//             send_log.refresh();
//         }
//     }
// }

fn processing(
    sent_channel: Receiver<(u64, Instant)>,
    recv_channel: Receiver<(u64, u16, Instant)>,
    rept_channel: Sender<Report>,
    max_delay: Duration,
) {
    let mut sent_time_cache = HashMap::new();
    let mut recv_unique_mapset = HashMap::<u64, HashSet<u16>>::new();
    let mut all_delays = HashMap::new();
    let mut seq_order = VecDeque::new();

    loop {
        // process one receive time
        while let Ok((seq, index, recv_time)) = recv_channel.recv() {
            // first check for new sent_times
            while let Ok((seq, sent_time)) = sent_channel.try_recv() {
                sent_time_cache.insert(seq, sent_time);
                seq_order.push_back((seq, sent_time));
            }

            // process only if not already received
            if !already_received(seq, index, &recv_unique_mapset) {
                if let Some(sent_time) = sent_time_cache.get(&seq) {
                    // calculate delay
                    let delay = recv_time.duration_since(*sent_time);
                    if delay < max_delay {
                        all_delays
                            .entry(seq)
                            .or_insert_with(Vec::new)
                            .push(delay.as_micros());
                        insert_mapset(seq, index, &mut recv_unique_mapset);
                    } else {
                        // println!("miniframe ({} | {}) too late", seq, index);
                        recv_unique_mapset.remove(&seq);
                    }
                }
            }

            // remove expired requests
            // let reporting_now = Instant::now();
            let mut remove_front = false;
            if let Some((seq, sent_time)) = seq_order.front() {
                if Instant::elapsed(&sent_time) > Duration::new(1, 0) {
                    remove_front = true;

                    // create report
                    let mut report = Report {
                        delay_sum: 0,
                        delay_amt: 0,
                    };

                    if let Some(vec) = all_delays.remove(&seq) {
                        let delay_sum = vec.iter().sum::<u128>() as u64;
                        let delay_amt = vec.len() as u64;
                        report = Report {
                            delay_sum,
                            delay_amt,
                        };
                    }

                    // send report
                    rept_channel.send(report).unwrap();

                    // collect garbage
                    sent_time_cache.remove(&seq);
                    recv_unique_mapset.remove(&seq);
                }
            }
            if remove_front {
                // let (seq, _) = seq_order.pop_front().unwrap();
                seq_order.pop_front().unwrap();
                // println!(
                //     "reporting sequence {} took {:?}",
                //     seq,
                //     Instant::elapsed(&reporting_now)
                // );
            }

            // println!(
            //     "processing sequence ({}|{}) took {:?}",
            //     seq,
            //     index,
            //     Instant::elapsed(&now)
            // );
        }

        thread::sleep(Duration::new(0, 1_000));
    }
}

fn already_received(seq: u64, index: u16, mapset: &HashMap<u64, HashSet<u16>>) -> bool {
    if let Some(set) = mapset.get(&seq) {
        set.contains(&index)
    } else {
        false
    }
}

fn insert_mapset(seq: u64, index: u16, mapset: &mut HashMap<u64, HashSet<u16>>) {
    mapset.entry(seq).or_insert_with(HashSet::new).insert(index);
}
