extern crate clap;
extern crate rand;
extern crate threadpool;

use clap::{App, Arg};
use rand::seq::SliceRandom;
use threadpool::ThreadPool;

use libcloudvr::response::ResponseIterator;
use libcloudvr::FrameRequest;

use std::fs::File;
use std::io::{BufRead, BufReader};
use std::net::{SocketAddr, UdpSocket};
use std::sync::Arc;
use std::thread::sleep;
use std::time::Duration;

fn main() -> std::io::Result<()> {
    let matches = App::new("cloudvr-server")
        .version("0.1")
        .author("Erik van Bennekum")
        .about("Cloud VR emulator")
        .args(&[
            Arg::with_name("threads")
                .about("sets the config file to use")
                .default_value("4")
                .short('t'),
            Arg::with_name("socket")
                .about("The UDP port to to server")
                .default_value("127.0.0.1:9999")
                .short('s'),
            Arg::with_name("pixels")
                .about("width and height of miniframe <= 20")
                .default_value("20")
                .short('p'),
            Arg::with_name("computation")
                .about("computational delay in microseconds")
                .default_value("0")
                .short('c'),
            Arg::new("delays")
                .short('d')
                .value_name("FILE")
                .about("Processing delay randomly sampled from FILE"),
        ])
        .get_matches();

    /* PROCESS COMMAND ARGUMENTS */
    // socket parsing
    let socket_address: SocketAddr = matches.value_of_t("socket").unwrap();
    let socket = UdpSocket::bind(socket_address).expect("Could not bind to this socket");

    // number of threads parsing
    let number_of_threads: usize = matches.value_of_t("threads").unwrap();
    let pool = ThreadPool::with_name("request-handle-pool".to_string(), number_of_threads);

    // miniframe pixels parsing
    let miniframe_pixels = matches.value_of_t("pixels").unwrap();
    assert!(
        miniframe_pixels <= 20,
        "Option pixels should be equal or smaller than 20"
    );

    // random processing delay
    let mut delays = Vec::new();
    if let Some(filename) = matches.value_of("delays") {
        let file = File::open(filename).expect("Could not open file");
        let reader = BufReader::new(file);

        // Read the file line by line using the lines() iterator from std::io::BufRead.
        for line in reader.lines() {
            let line = line.unwrap();
            let delay = line.parse::<u64>().expect("file contains non-integer data");
            delays.push(delay);
        }
    }
    // println!("delay: {:?}", delays);
    let delays = Arc::new(delays);

    // computational delay
    let computation: u64 = matches.value_of_t("computation").unwrap();

    println!("Server started on port {:?}", socket_address.port());

    let mut buf = [0; 1500];
    while let Ok((amt, src)) = socket.recv_from(&mut buf) {
        if amt == std::mem::size_of::<FrameRequest>() {
            // let req = FrameRequest::create(&buf).unwrap();
            let req = bincode::config().big_endian().deserialize(&buf).unwrap();
            // let socket_handle = socket.try_clone().unwrap();
            let delays = delays.clone();
            pool.execute(move || {
                // let now = Instant::now();

                handle_request(req, src, miniframe_pixels, computation, &delays);
                // println!("sending response took : {:?}", Instant::elapsed(&now));
            });
        }
    }

    Ok(())
}

fn handle_request(
    request: FrameRequest,
    source: SocketAddr,
    // socket: UdpSocket,
    miniframe_pixels: usize,
    computation: u64,
    delays: &[u64],
) {
    let socket = UdpSocket::bind("0.0.0.0:0").expect("Could not bind to this socket");
    socket.connect(source).unwrap();
    // let now = Instant::now();
    let response_iterator = ResponseIterator::new(request, miniframe_pixels);
    // let bytecount = response_iterator.m_bytes * response_iterator.stop as usize;

    // emulate the fixed/random processing delay
    if computation > 0 {
        sleep(Duration::from_micros(computation));
    } else if !delays.is_empty() {
        let delay = delays.choose(&mut rand::thread_rng()).unwrap();
        // println!("sleeping for {} microseconds", delay);
        sleep(Duration::from_micros(*delay));
    }

    for bytes in response_iterator {
        // let now2 = Instant::now();
        let slice = bytes.as_slice();
        // println!("slicing: {:?}", Instant::elapsed(&now2));
        // let now3 = Instant::now();
        match socket.send(slice) {
            Ok(_) => (),
            Err(err) => {
                println!("sending error: {:?}", err);
                break;
            }
        };
    }
    // let duration = Instant::elapsed(&now);
    // let mbps = (8 * bytecount) as f64 / duration.as_secs_f64() / 1_000_000f64;
    // println!("mbps: {}", mbps);
}
