use socket2::{Domain, Protocol, SockAddr, Socket, Type};
use std::net::SocketAddr;
use std::time::Instant;
const BYTE_AMT: usize = 1218;

fn main() {
    let recv_addr: SockAddr = "127.0.0.1:10000".parse::<SocketAddr>().unwrap().into();
    let send_addr: SockAddr = "127.0.0.1:19000".parse::<SocketAddr>().unwrap().into();

    let recv_socket = Socket::new(Domain::ipv4(), Type::dgram(), Some(Protocol::udp())).unwrap();
    let send_socket = Socket::new(Domain::ipv4(), Type::dgram(), Some(Protocol::udp())).unwrap();
    // send_socket.set_send_buffer_size(26214400).unwrap();
    // send_socket.set_send_buffer_size(200).unwrap();
    recv_socket.bind(&recv_addr).unwrap();

    send_socket.bind(&send_addr).unwrap();
    send_socket.connect(&recv_addr).unwrap();

    println!(
        "send buffer size {}",
        send_socket.send_buffer_size().unwrap()
    );

    let now = Instant::now();

    let bytes = [0; BYTE_AMT];
    let loops = 1_000_000;
    for _ in 0..loops {
        send_socket.send(&bytes).unwrap();
    }
    let elapsed = Instant::elapsed(&now);
    println!("{:?}", elapsed);
    let seconds = elapsed.as_secs_f64();
    println!("seconds: {}", seconds);
    let mbits = BYTE_AMT * 8 * loops / 1_000_000;
    println!("bits: {}", mbits);
    let mbps = mbits as f64 / seconds;
    println!("total mbits over udp per second: {}", mbps);
}
